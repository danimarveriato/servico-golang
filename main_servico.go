package main

//Bibliotecas externas
import (
	"bufio"
	"database/sql"
	"errors"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	_ "github.com/lib/pq"
)

//Variáveis de conexão com o banco de dados
const (
	host     = "servicos_teste.postgresql.dbaas.com.br"
	port     = 5432
	user     = "servicos_teste"
	password = "bd-serv-2020"
	dbname   = "servicos_teste"
)

func main() {
	//Cria a flag para pegar o nome do arquivo por parâmetro
	var fileName string
	flag.StringVar(&fileName, "fileName", "", "a string var")
	flag.Parse()

	//Se não foi informado, solicita ao usuário para informar o nome do arquivo como parâmetro
	if fileName == "" {
		fmt.Println("AVISO: Informe o nome do arquivo a ser importado para o Banco de Dados. \nEx. chamada: main_servico -fileName=\"teste.txt\" ")
	} else {
		// Abre o arquivo
		arquivo, err := os.Open(fileName)
		// Caso tenha encontrado algum erro ao tentar abrir o arquivo retorna o erro encontrado
		if err != nil {
			fmt.Println(err)
		}
		// Garante que o arquivo é fechado após o uso
		defer arquivo.Close()

		// Cria um scanner que le cada linha do arquivo
		var header bool //Guarda a primeira linha do arquivo (cabeçalho)
		scanner := bufio.NewScanner(arquivo)
		for scanner.Scan() {
			if !header {
				header = true
				continue
			}

			//Quebra a linha em colunas
			colunas := strings.Fields(scanner.Text())
			cpf := colunas[0]
			private := colunas[1]
			incompleto := colunas[2]
			dtUltCompra := colunas[3]

			//Substitui a vírgula por ponto nos campos de valor monetário
			ticketMedio := strings.Replace(colunas[4], ",", ".", -1)
			ticketUltCompra := strings.Replace(colunas[5], ",", ".", -1)
			lojaFrequente := colunas[6]
			lojaUltCompra := colunas[7]

			//Valida o CPF informado
			errCPF := ValidaCPF(cpf)

			//Verifica se os CNPJs são válidos
			errLojaFreq := validaCNPJ(lojaFrequente)
			errUltCompra := validaCNPJ(lojaUltCompra)

			//Se o CPF e os CNPJs da linha são válidos (== NIL), insere o registro no banco
			if errLojaFreq == nil && errUltCompra == nil && errCPF == nil {
				//Chama a função que conecta no banco e insere o registro no banco
				connPGsql(cpf, private, incompleto, dtUltCompra, ticketMedio, ticketUltCompra, lojaFrequente, lojaUltCompra)
			} else {
				fmt.Println("O registro do CPF n° " + cpf + " contém o CPF e/ou CNPJs inválidos.")
			}
		}
	}
}

//Função de conexão com o PostgreSQL
func connPGsql(pCpf string, pPriv string, pInc string, pDtUltCompra string, pVlrTicketMed string, pTicketUltCompra string, pLojaFreq string, pLojaUltCompra string) {
	//String com os dados de conexão
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	//Abre a conexão com o banco
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		panic(err)
	}

	//Monta o SQL para insert na tabela
	sqlStatement1 := " INSERT INTO servico "
	sqlStatement1 += "   (cpf, "
	sqlStatement1 += "   private, "
	sqlStatement1 += "   incompleto, "
	sqlStatement1 += "   dt_ult_compra, "
	sqlStatement1 += "   vlr_medio, "
	sqlStatement1 += "   vlr_ult_compra, "
	sqlStatement1 += "   loja_frequente, "
	sqlStatement1 += "   loja_ult_compra) "
	sqlStatement1 += " VALUES ('" + pCpf + "', "
	sqlStatement1 += " " + pPriv + ", "
	sqlStatement1 += " " + pInc + ", "
	//Trata quando o valor informado é nulo
	if pDtUltCompra == "NULL" {
		sqlStatement1 += " " + pDtUltCompra + ", "
	} else {
		sqlStatement1 += " '" + pDtUltCompra + "', "
	}
	sqlStatement1 += " " + pVlrTicketMed + ", "
	sqlStatement1 += " " + pTicketUltCompra + ", "
	sqlStatement1 += " '" + pLojaFreq + "', "
	sqlStatement1 += " '" + pLojaUltCompra + "') "

	//Mostra na tela o comando SQL a ser executado
	println(sqlStatement1)
	//Executa o comando SQL
	_, err = db.Exec(sqlStatement1)

	if err != nil {
		panic(err)
	} else {
		fmt.Println("Registro inserido com sucesso!")
	}
	//Fecha a conexão com o banco
	defer db.Close()
}

//Função que valida o CPF informado. Caso for válido, retorna NIL
func ValidaCPF(cpf string) error {
	cpf = strings.Replace(cpf, ".", "", -1)
	cpf = strings.Replace(cpf, "-", "", -1)
	if len(cpf) != 11 {
		return errors.New("CPF inválido")
	}
	var eq bool
	var dig string
	for _, val := range cpf {
		if len(dig) == 0 {
			dig = string(val)
		}
		if string(val) == dig {
			eq = true
			continue
		}
		eq = false
		break
	}
	if eq {
		return errors.New("CPF inválido")
	}

	i := 10
	sum := 0
	for index := 0; index < len(cpf)-2; index++ {
		pos, _ := strconv.Atoi(string(cpf[index]))
		sum += pos * i
		i--
	}

	prod := sum * 10
	mod := prod % 11
	if mod == 10 {
		mod = 0
	}
	digit1, _ := strconv.Atoi(string(cpf[9]))
	if mod != digit1 {
		return errors.New("CPF inválido")
	}
	i = 11
	sum = 0
	for index := 0; index < len(cpf)-1; index++ {
		pos, _ := strconv.Atoi(string(cpf[index]))
		sum += pos * i
		i--
	}
	prod = sum * 10
	mod = prod % 11
	if mod == 10 {
		mod = 0
	}
	digit2, _ := strconv.Atoi(string(cpf[10]))
	if mod != digit2 {
		return errors.New("CPF inválido")
	}

	return nil
}

//Função que valida o CNPJ. Caso for válido, retorna NIL
func validaCNPJ(cnpj string) error {
	cnpj = strings.Replace(cnpj, ".", "", -1)
	cnpj = strings.Replace(cnpj, "-", "", -1)
	cnpj = strings.Replace(cnpj, "/", "", -1)
	if len(cnpj) != 14 {
		return errors.New("CNPJ inválido")
	}

	algs := []int{5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2}
	var algProdCpfDig1 = make([]int, 12, 12)
	for key, val := range algs {
		intParsed, _ := strconv.Atoi(string(cnpj[key]))
		sumTmp := val * intParsed
		algProdCpfDig1[key] = sumTmp
	}
	sum := 0
	for _, val := range algProdCpfDig1 {
		sum += val
	}
	digit1 := sum % 11
	if digit1 < 2 {
		digit1 = 0
	} else {
		digit1 = 11 - digit1
	}
	char12, _ := strconv.Atoi(string(cnpj[12]))
	if char12 != digit1 {
		return errors.New("CNPJ inválido")
	}
	algs = append([]int{6}, algs...)

	var algProdCpfDig2 = make([]int, 13, 13)
	for key, val := range algs {
		intParsed, _ := strconv.Atoi(string(cnpj[key]))

		sumTmp := val * intParsed
		algProdCpfDig2[key] = sumTmp
	}
	sum = 0
	for _, val := range algProdCpfDig2 {
		sum += val
	}

	digit2 := sum % 11
	if digit2 < 2 {
		digit2 = 0
	} else {
		digit2 = 11 - digit2
	}
	char13, _ := strconv.Atoi(string(cnpj[13]))
	if char13 != digit2 {
		return errors.New("CNPJ inválido")
	}

	return nil
}
