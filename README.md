# Servico Golang

Serviço de manipulação de dados e persistência no banco de dados PostgreSQL utilizando a Linguagem Go.



# Banco de Dados
Foi criado uma base de dados específica para a execução desta tarefa utilizando o PostgreSQL v9.55 hospedado na Locaweb.


--Script de criação da tabela
CREATE TABLE servico (
	cpf VARCHAR(14) NOT NULL,
	private INTEGER NOT NULL,
	incompleto INTEGER NOT NULL,
	dt_ult_compra DATE,
	vlr_medio NUMERIC(10,2),
	vlr_ult_compra NUMERIC(10,2),
	loja_frequente VARCHAR(18),
	loja_ult_compra VARCHAR(18),	
	PRIMARY KEY (cpf)
);


# Instalação e Configuração do Ambiente:

O primeiro passo para a execução do serviço, é a instalação da Linguagem Go na máquina através do link: https://golang.org/dl/.
Neste exemplo vamos utilizar a instalação para a plataforma Windows.

Após a instalação, a configuração é basicamente a criação de uma pasta chamada "go" no usuário logado no sistema. 
No exemplo, o diretório criado ficou assim: "C:\Users\danimar\go". 
Após a criação desta pasta, basta baixar e colar as pastas bin, pkg e src contidas neste repositório.

Para verificar se a Linguagem Go foi instalada com sucesso na máquina, abra o terminal do Windows (Windows + R e digite cmd) e digite "go version" sem as aspas e deve aparecer a versão instalada na máquina, no meu caso está na versão go1.13.6 x64.


# Executando o Serviço

Para a execução do servico na linguagem go, abra o terminal do windows e navegue até a pasta /src/servico.
Feito isso, precisamos compilar o arquivo main_servico.go e executá-lo.
Para compilar o arquivo, digite no terminal o seguinte comando: go build main_servico.go
Com isso, foi gerado um novo arquivo no diretório /src/servico chamado "main_servico.exe".

Precisamos agora executar este arquivo, passando como parâmetro o nome do arquivo que contém os registros que queremos inserir no banco de dados.
Para isso, execute no terminal o seguinte comando: main_servico -fileName="base_teste.txt" onde -fileName é o nome do parâmetro que identifica o arquivo com os dados que queremos importar, e "base_teste.txt" é o nome do arquivo contendo os dados (é o arquivo que está junto neste diretório).

A seguir o serviço comecará a executar, onde exibirá no terminal o comando SQL do insert e logo abaixo a mensagem "Registro inserido com sucesso!" para cada linha inserida no banco.
Detalhe importante que o serviço faz a validação dos dados de cada linha, e insere na base de dados apenas os registros que contém CPF e CNPJs válidos.

Para fins de testes, a tabela no banco de dados foi esvaziada, sendo assim, é possível realizar uma nova execução do serviço, uma vez que a PK da tabela é o cpf contido na primeira coluna do nosso arquivo base_teste.txt.